/*
 * @Author: Pzq
 * @Date: 2023-04-17 17:07:19
 * @LastEditors: Pzq
 * @LastEditTime: 2023-04-17 17:14:44
 * @FilePath: \git_learn\src\Point.cpp
 */
#include "Point.h"

Point::Point(int xx, int yy, int zz) {
  this->x = xx;
  this->y = yy;
  this->z = zz;
  std::cout << "Point's Constructor." << std::endl;
}

Point::~Point() { std::cout << "Point's Destructor." << std::endl; }

void Point::showPoint() {
    std::cout << "X:" << this->x << "|" << "Y:" << this->y << "|" << "Z:"<< this->z<< std::endl;
}
