#ifndef __POINT_H__
#define __POINT_H__

#include <iostream>
class Point {
 private:
  /* data */
  int x, y, z;

 public:
  Point(int xx, int yy, int zz);
  ~Point();
  void showPoint();
};
#endif  // __POINT_H__